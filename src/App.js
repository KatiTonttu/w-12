import logo from './flower.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="card">
        <img src={logo} alt="flower logo" width="200"/>
        <h1><span className="color">Studio Asphodel</span></h1>
        <br></br>
        <h4><span className="color">Malagankatu 2 A2, 00220, Helsinki</span></h4>
        <h4><span className="color">Kati Jutila</span></h4>
        <h4><span className="color">kati.jutila@gmail.com</span></h4>
        <h4><span className="color">+358 405864574</span></h4>
      </div>
    </div>
  );
}

export default App;
